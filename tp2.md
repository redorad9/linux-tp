# Partie 1 : Mise en place de la solution

## Sommaire

[[_TOC_]]

# I. Setup base de données

## 1. Install MariaDB
```bash
[michael@db ~]$ sudo dnf install mariadb-server
[michael@db ~]$ sudo systemctl start mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 15:53:38 CET; 21s ago

[michael@db ~]$ sudo systemctl enable mariadb

[michael@db ~]$ ss -ltptn
State          Recv-Q         Send-Q                   Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                            0.0.0.0:22                          0.0.0.0:*
LISTEN         0              80                                   *:3306                              *:*
LISTEN         0              128                               [::]:22                             [::]:*
#PORT 3306

[michael@db ~]$ ps -aux | grep mariadb
michael     5331  0.0  0.1 221928  1156 pts/0    S+   15:56   0:00 grep --color=auto mariadb
#mariaDB lancé par l'utilisateur michael

[michael@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success

[michael@db ~]$ sudo firewall-cmd --reload
success

```
## 2. Conf MariaDB
```bash
[michael@db ~]$ sudo mysql_secure_installation
###YES TO ALL QUESTIONS

[michael@db ~]$ sudo mysql -u root -p
MariaDB [(none)]> CREATE USER'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
MariaDB [(none)]> FLUSH PRIVILEGES;
```
## 3. Test
```bash
[michael@web ~]$ sudo dnf install mysql
[michael@web ~]$ mysql -h 10.2.1.12 -u nextcloud -p

mysql> USE nextcloud
Database changed
mysql> SHOW FULL TABLES;
Empty set (0.00 sec)
```
# II. Setup Apache

## 1. Install Apache
```bash
[michael@web ~]$ sudo dnf install httpd
[michael@web ~]$ sudo systemctl start httpd
[michael@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[michael@web ~]$ ps -aux | grep httpd
root        3237  0.0  1.4 282936 11840 ?        Ss   17:05   0:00 /usr/sbin/httpd -DFOREGROUND
apache      3238  0.0  1.0 296820  8792 ?        S    17:05   0:00 /usr/sbin/httpd -DFOREGROUND
apache      3239  0.0  2.0 2534252 16388 ?       Sl   17:05   0:00 /usr/sbin/httpd -DFOREGROUND
apache      3240  0.0  2.3 2796464 18432 ?       Sl   17:05   0:00 /usr/sbin/httpd -DFOREGROUND
apache      3241  0.0  2.0 2599788 16388 ?       Sl   17:05   0:00 /usr/sbin/httpd -DFOREGROUND
michael     3480  0.0  0.1 221928  1080 pts/0    S+   17:06   0:00 grep --color=auto httpd
PORT 80

[michael@db ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success

[michael@db ~]$ sudo firewall-cmd --reload
success

[michael@web ~]$ curl 192.168.127.130
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
[…]
<a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>

```
## 2. Conf Apache
```bash
[michael@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep conf.d
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf


[michael@web ~]$ sudo systemctl restart httpd

[michael@web ~]$ sudo chown -R apache /var/www/nextcloud/html
[michael@web ~]$ sudo timedatectl
               Local time: Wed 2021-12-15 09:15:58 CET
           Universal time: Wed 2021-12-15 08:15:58 UTC
                 RTC time: Wed 2021-12-15 08:15:59
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no

Mettre dans le fichier /etc/opt/remi/php74/php.ini à la ligne ;date.timezone : 
	• ;date.timezone = "Europe/Paris"
```	
# III. NextCloud
```bash
[michael@web ~]$ curl -SLOhttps://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[michael@web ~]$ sudo unzip nextcloud-21.0.1.zip  -d /var/www/nextcloud/html

[michael@web ~]$ rm nextcloud-21.0.1.zip
[michael@web ~]$ sudo chown apache nextcloud
```
## 4 Test
```bash
PS C:\WINDOWS\system32> nodepad.exe c:\windows\system32\drivers\etc\hosts
10.2.1.11 web.tp2.cesi

[michael@web ~]$ sudo chown -R apache:apache /var/www/nextcloud/

http://web.tp2.cesi/index.php/apps/dashboard/
```
# Partie 2 : Sécurisation

## I. Serveur SSH

### 1 Conf SSH
```

[michael@web ~]$ sudo nano /etc/ssh/sshd_config
#Modify those :
PermitRootLogin no
PasswordAuthentication no
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256

[michael@web ~]$ sudo sshd -T
[michael@web ~]$ sudo service sshd reload
```
### 2. Bonus : Fail2Ban
```bash

[michael@web ~]$ sudo dnf install epel-release -y
[michael@web ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
[michael@web ~]$ sudo systemctl start fail2ban
[michael@web ~]$ sudo systemctl enable fail2ban
[michael@web ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
[michael@web ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
[michael@web ~]$ sudo systemctl restart fail2ban

[michael@web ~]$ sudo nano /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
bantime = 1d
maxretry = 3

[michael@web ~]$ sudo systemctl restart fail2ban

SUPPRIMER LA CLE SUR ~/.ssh/authorized_keys
REMETTRE PasswordAuthentication yes

PS C:\Users\micha> ssh michael@192.168.127.130
michael@192.168.127.130's password:
Permission denied, please try again.
michael@192.168.127.130's password:
Permission denied, please try again.
michael@192.168.127.130's password:
michael@192.168.127.130: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
PS C:\Users\micha> ssh michael@192.168.127.130
ssh: connect to host 192.168.127.130 port 22: Connection timed out

[michael@web ~]$ sudo fail2ban-client unban 192.168.127.1
```
## II. Serveur Web

### 1. Reverse Proxy
```bash
[michael@proxy ~]$ sudo systemctl start nginx
[michael@proxy ~]$ sudo systemctl enable nginx

[michael@proxy ~]$ sudo nano /etc/nginx/conf.d/sololeveling.conf
[michael@proxy ~]$ sudo systemctl restart nginx

server {
        listen 80;
        server_name web.tp2.cesi;

        location /nextcloud {
                proxy_pass http://192.168.127.128;
        }
}

```
### 2. HTTP
```bash
[michael@proxy ~]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
...................++++
.............................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Aquitaine
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:Michael
Organizational Unit Name (eg, section) []:section
Common Name (eg, your name or your servers hostname) []:web
Email Address []:xdefalque@gmail.com

[michael@proxy ~]$ sudo mv web.tp2.cesi.key /etc/pki/tls/private/web.tp2.cesi.key
[michael@proxy ~]$ sudo mv web.tp2.cesi.crt /etc/pki/tls/certs/web.tp2.cesi.crt

[michael@proxy ~]$ curl -L -k web.tp2.cesino /etc/resolv.conf
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="mOSVMFrgLkw1VWqnwmJMkx2w1up3rdIYzim0i8vYOqE=:7LbUYB6YSGdZYyaWjTQL2i7JuZodw5ZN90rdyqyPTdA=">
[…]
<footer role="contentinfo">
                        <p class="info">
                                <a href="https://nextcloud.com" target="_blank" rel="noreferrer noopener" class="entity-name">Nextcloud</a> – a safe home for all your data                   </p>
                </footer>
        </body>
</html>
```
