# I. Préparation de la machine

## Sommaire

[[_TOC_]]

## 1 Réseau
```bash
[michael@node1 ~]$ ip a
[michael@node1 ~]$ cd /etc/sysconfig/network-scripts/
[michael@node1 ~]$ sudo nano ifcfg-ens192 #host-only

TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=ens192
UUID=1918e74b-2a52-452d-a26b-b4c4de42fd4e
DEVICE=ens192
ONBOOT=yes
IPADDR=192.168.127.130
NETMASK=255.255.255.0

[michael@node1 ~]$ sudo nano ifcfg-ens160 #nat
DNS1=1.1.1.1

[michael@node1 ~]$ sudo nmcli con reload
[michael@node1 ~]$ sudo nmcli con up ens192
[michael@node1 ~]$ sudo nmcli con up ens160

[michael@node1 ~]$ nano /etc/resolv.conf

[michael@node1 ~]$ ping www.google.com
PING www.google.com (216.58.201.228) 56(84) bytes of data.
64 bytes from fra02s18-in-f4.1e100.net (216.58.201.228): icmp_seq=1 ttl=128 time=15.4 ms
64 bytes from fra02s18-in-f4.1e100.net (216.58.201.228): icmp_seq=2 ttl=128 time=16.9 ms

[michael@node1 ~]$ sudo hostname node1.tp1.cesi

[michael@node1 ~]$ nano /etc/hostname

[michael@node1 ~]$ hostname
node1.tp1.cesi

```
## 2 Utilisateurs
```bash
[michael@node1 ~]$ sudo visudo /etc/sudoers
 Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL

 Same thing without a password
 %wheel        ALL=(ALL)       NOPASSWD: ALL

[michael@node1 ~]$ sudo useradd solo
[michael@node1 ~]$ sudo passwd solo
[michael@node1 ~]$ sudo usermod -a -G wheel solo
[michael@node1 ~]$ ls /home
michael  solo

```
## 3 SSH
```bash
Sur powershell :

PS C:\Users\micha> ssh michael@192.168.127.130
```
## 4 SELinux
```bash
[michael@node1 ~]$ sestatus
[michael@node1 ~]$ sudo seteforce 0
[michael@node1 ~]$ sudo nano /etc/selinux/config

[michael@node1 ~]$ sudo cat /etc/selinux/config

 This file controls the state of SELinux on the system.
 SELINUX= can take one of these three values:
     enforcing - SELinux security policy is enforced.
     permissive - SELinux prints warnings instead of enforcing.
     disabled - No SELinux policy is loaded.
SELINUX=permissive
 SELINUXTYPE= can take one of these three values:
     targeted - Targeted processes are protected,
     minimum - Modification of targeted policy. Only selected processes are protected.
     mls - Multi Level Security protection.
SELINUXTYPE=targeted
```
# II. Gestion de services

### 1 Analyse du service SSH
```bash
[michael@node1 ~]$ sudo systemctl status sshd
[sudo] password for michael:
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2021-12-13 11:56:20 CET; 1h 9min ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 1025 (sshd)
    Tasks: 1 (limit: 4770)
   Memory: 4.7M
   CGroup: /system.slice/sshd.service
           └─1025 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@o>

[michael@node1 ~]$ sudo ss -lutpn
Netid   State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp     LISTEN    0         128                0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=1025,fd=5))
tcp     LISTEN    0         128                   [::]:22                 [::]:*       users:(("sshd",pid=1025,fd=7))


[solo@node1 ~]$ ps -ef
solo        2030    2015  0 13:14 ?        00:00:00 sshd: solo@pts/0
```
### 2. Modification du service SSH
```bash
[solo@node1 ~]$ sudo nano /etc/ssh/sshd_config
Ajouter la ligne :
Port 6969

[solo@node1 ~]$ sudo systemctl restart sshd
[solo@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens160 ens192
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[solo@node1 ~]$ sudo firewall-cmd --permanent --add-port=6969/tcp
success
[solo@node1 ~]$ sudo firewall-cmd --reload
success
[solo@node1 ~]$ sudo ss -lutpn
Netid   State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port   Process
tcp     LISTEN    0         128                0.0.0.0:6969            0.0.0.0:*       users:(("sshd",pid=2094,fd=5))
tcp     LISTEN    0         128                   [::]:6969               [::]:*       users:(("sshd",pid=2094,fd=7))

PS C:\Users\micha> ssh -p 6969 solo@192.168.127.130
```
### 3 Service Web
```bash
[solo@node1 ~]$ sudo dnf install nginx
[solo@node1 ~]$ sudo systemctl start nginx
NGINX écoute sur le port 80

[solo@node1 ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
success

[solo@node1 ~]$ curl 192.168.127.130 #affiche sous format html
[solo@node1 ~]$ sudo firewall-cmd --reload
```
### 4 Votre propre service
```bash
[solo@node1 ~]$ sudo dnf install python3
[solo@node1 ~]$ sudo firewall-cmd --permanent --add-port=8888/tcp
Success
[solo@node1 ~]$ sudo firewall-cmd --reload
success
[solo@node1 ~]$ sudo python3 -m http.server 8888
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.127.1 - - [13/Dec/2021 14:12:09] "GET / HTTP/1.1" 200 -
192.168.127.1 - - [13/Dec/2021 14:12:10] code 404, message File not found
192.168.127.1 - - [13/Dec/2021 14:12:10] "GET /favicon.ico HTTP/1.1" 404 -

[solo@node1 ~]$ sudo systemctl daemon-reload

[solo@node1 system]$ sudo nano web.service
[Unit]
Description=test

[Service]
ExecStart= /usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

[solo@node1 ~]$ sudo systemctl daemon-reload
[solo@node1 ~]$ sudo systemctl start web.service

[solo@node1 ~]$ systemctl status web.service
● web.service - test
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-12-13 14:34:11 CET; 3s ago
 Main PID: 38769 (python3)
    Tasks: 1 (limit: 4770)
   Memory: 9.4M
   CGroup: /system.slice/web.service
           └─38769 /usr/bin/python3 -m http.server 8888


[solo@node1 ~]$ sudo useradd web
[solo@node1 ~]$ sudo passwd web
[solo@node1 ~]$ sudo mkdir /srv/web/
[solo@node1 web]$ sudo nano web #écrire un truc
[solo@node1 web]$ sudo chown web web
[solo@node1 web]$ ls -al
total 4
drwxr-xr-x. 2 root root 17 Dec 13 14:48 .
drwxr-xr-x. 3 root root 17 Dec 13 14:38 ..
-rw-r--r--. 1 web  root 38 Dec 13 14:42 web

[solo@node1 system]$ curl 192.168.127.130
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
[…]
      </div>
    </div>
  </body>
</html>

```
# Partie 3 : Bonus
### I. Echange de clés SSH
```bash
PS C:\WINDOWS\system32> ssh-keygen -t ed25519

Passphrase : sololeveling

Your identification has been saved in C:\Users\micha/.ssh/id_ed25519.
Your public key has been saved in C:\Users\micha/.ssh/id_ed25519.pub.
The key fingerprint is:
SHA256:w0ffIr/bDdeyCcV+imSGhAgx4QXFLukj5ICtkiOgRfI micha@DESKTOP-8RT50IP
The key's randomart image is:
+--[ED25519 256]--+
|    ==o          |
|. .. +.          |
|.=  oo    .      |
|+ E o..o o . o   |
|oB . .. S + o +  |
|B o o    + + +  .|
|o. . .    . * + +|
|           + = X |
|            +.= .|
+----[SHA256]-----+

[solo@node1 ~]$ ls ~/.ssh

[solo@node1 ~]$ cat ~/.ssh/id_ed25519.pub | ssh solo@192.168.127.130 "cat >> ~/.ssh/authorized_keys"

ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJlATA0paCt6CfzjZkudSmXlfCi58iEzyBJIkPT/ZNKF micha@DESKTOP-8RT50IP

Mettre la clé publique crée sur le client pour le mettre dans le fichier ~/.ssh/authorized_keys du serveur distant
[solo@node1 ~]$ chmod 700 ~/.ssh/authorized_keys

PS C:\WINDOWS\system32> ssh -p 6969 solo@192.168.127.130
Enter passphrase for key 'C:\Users\micha/.ssh/id_ed25519':
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Dec 13 15:33:08 2021 from 192.168.127.1
*[solo@node1 ~]$

ssh-add 
```
